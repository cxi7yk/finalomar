#include "wind.h"
#include <string.h>

static const char *TAG = "WindSpeedSensor";

static SemaphoreHandle_t wind_speed_mutex;
static WindSpeedData wind_speed_data = { 0 };

void IRAM_ATTR wind_speed_isr_handler(void *arg) {
	WindSpeedData *wind_speed_data = (WindSpeedData*) arg;

	xSemaphoreTakeFromISR(wind_speed_mutex, NULL);
	wind_speed_data->impulse_count++;
	xSemaphoreGiveFromISR(wind_speed_mutex, NULL);
}

void wind_speed_task(void *pvParameters) {
	while (1) {
		vTaskDelay(pdMS_TO_TICKS(1000));  // 1 second delay

		xSemaphoreTake(wind_speed_mutex, portMAX_DELAY);

		// Calculate wind speed
		wind_speed_data.wind_speed_kmh = wind_speed_data.impulse_count * 0.36;

		// Reset the impulse count after calculating wind speed
		wind_speed_data.impulse_count = 0;

		xSemaphoreGive(wind_speed_mutex);

		ESP_LOGI(TAG, "Wind Speed: %.2f km/h", wind_speed_data.wind_speed_kmh);
	}
}

int get_wind_speed(int argc, char **argv) {
	xSemaphoreTake(wind_speed_mutex, portMAX_DELAY);

	WindSpeedData local_wind_speed_data;
	memcpy(&local_wind_speed_data, &wind_speed_data, sizeof(WindSpeedData));

	xSemaphoreGive(wind_speed_mutex);

	ESP_LOGI(TAG, "Wind Speed: %.2f km/h",
			local_wind_speed_data.wind_speed_kmh);

	return 0;
}

int reset_wind_speed(int argc, char **argv) {
	xSemaphoreTake(wind_speed_mutex, portMAX_DELAY);

	// Reset the impulse count
	wind_speed_data = (WindSpeedData ) { 0 };

	xSemaphoreGive(wind_speed_mutex);

	ESP_LOGI(TAG, "Wind Speed Reset");

	return 0;
}

void register_commands() {
	const esp_console_cmd_t get_wind_speed_cmd = { .command = "get_wind_speed",
			.help = "Get the current wind speed", .hint = NULL, .func =
					&get_wind_speed, };

	const esp_console_cmd_t reset_wind_speed_cmd = { .command =
			"reset_wind_speed", .help = "Reset the wind speed", .hint = NULL,
			.func = &reset_wind_speed, };

	ESP_ERROR_CHECK(esp_console_cmd_register(&get_wind_speed_cmd));
	ESP_ERROR_CHECK(esp_console_cmd_register(&reset_wind_speed_cmd));
}

void wind_speed_sensor_init() {
	wind_speed_mutex = xSemaphoreCreateMutex();

	gpio_config_t io_conf = { .pin_bit_mask = (1ULL << WIND_SENSOR_PIN), .mode =
			GPIO_MODE_INPUT, .intr_type = GPIO_INTR_ANYEDGE, .pull_down_en =
			GPIO_PULLDOWN_DISABLE, .pull_up_en = GPIO_PULLUP_ENABLE, };

	gpio_config(&io_conf);

	gpio_install_isr_service(0);
	gpio_isr_handler_add(WIND_SENSOR_PIN, wind_speed_isr_handler,
			&wind_speed_data);

	xTaskCreate(&wind_speed_task, "wind_speed_task", 4096, NULL, 5, NULL);

	register_commands();
}


#ifndef MQTT_H
#define MQTT_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

esp_err_t mac_get_address(char *buffer, size_t size);
BaseType_t mqtt_init();


BaseType_t mqtt_deinit();


BaseType_t send_mqtt(const char *topic, char *msg);

#endif /* MQTT_H */

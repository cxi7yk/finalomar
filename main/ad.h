/*
 * ad.h
 *
 *  Created on: Nov 6, 2023
 *      Author: osta
 */

#ifndef MAIN_AD_H_
#define MAIN_AD_H_

#ifndef AD_H
#define AD_H

#include <stdint.h>
#include <stdbool.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"

#define JSON_LABEL_TEMPERATURE "temperature"
#define TOPIC_OBIS "tempreture"
#define JSON_LABEL_ESP_ADDRESS "espAddress"

BaseType_t ad_init();
BaseType_t ad_deinit();
BaseType_t ad_get(int channel, float *value, TickType_t wait);

#endif // AD_H

#endif /* MAIN_AD_H_ */

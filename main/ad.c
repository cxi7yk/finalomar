#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "esp_console.h"
#include "argtable3/argtable3.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "cJSON.h"
#include "esp_log.h"
#include "esp_adc/adc_oneshot.h"
#include "esp_err.h"
#include "esp_efuse.h"

#include "ad.h"

#define DEBUG // TODO don't forget to set undef!!!

#define MAX_CHANNELS 2

#undef LOG_LOCAL_LEVEL
#define LOG_LOCAL_LEVEL ESP_LOG_DEBUG
#define TAG "ad"

#define CHAN0    ADC_CHANNEL_0
#define CHAN1    ADC_CHANNEL_2
#define ADC_ATTEN    ADC_ATTEN_DB_11

#define MOVING_AVG_QUEUE_SIZE 5

#define MOVING_HIST_DELTA 8

#define AD_CONVERSION_MS 100

#define AD_RAW_MIN 1   // ADC raw value minimum
#define AD_RAW_MAX 4096  // ADC raw value maximum

#define INNER_TEMP_MIN 10    // Minimum temperature for the inner sensor
#define INNER_TEMP_MAX 30    // Maximum temperature for the inner sensor

#define OUTER_TEMP_MIN -30   // Minimum temperature for the outer sensor
#define OUTER_TEMP_MAX 40    // Maximum temperature for the outer sensor

#define NVS_KEY_D0 "d0"
#define NVS_KEY_D1 "d1"
#define NVS_KEY_T0 "t0"
#define NVS_KEY_T1 "t1"
#define NVS_KEY_TG_ALPHA "tga"
#define NVS_NAME_SPACE "ad_storage"

adc_channel_t Channel_IDS[MAX_CHANNELS] = { ADC_CHANNEL_0, ADC_CHANNEL_2 };
static adc_oneshot_unit_handle_t adc_handle;

typedef union {
	uint8_t as_bytes[6];
	uint64_t as_long;
} mac_t;
typedef struct {
	uint16_t avg[MOVING_AVG_QUEUE_SIZE];
	uint8_t index;
} moving_avg_t;

typedef struct {
	uint16_t min;
	uint16_t max;
	uint16_t delta;
} moving_hist_t;

typedef struct {
	uint16_t d0;
	uint16_t d1;
	float temp_min;
	float temp_max;
	float tg_alpha;
	bool valid;
} calibration_t;

static struct {
	int raw;
	uint16_t normalized;
	float thermal_value;
	moving_avg_t moving_avg;
	moving_hist_t moving_hist;
	uint32_t ad_errors;
	calibration_t calibration;
	SemaphoreHandle_t semaphore;
	uint32_t semaphore_error;
} ad[MAX_CHANNELS];

static int register_cmd();

static inline int chk_ch(int ch) {
	if (ch < 0 || ch >= MAX_CHANNELS) {
		printf("Channel must be between 0 up to %d\r\n", MAX_CHANNELS);
		return 0;
	}
	return 1;
}

static uint16_t moving_avg(uint16_t in, int channel) {
	if (!chk_ch(channel)) {
		return 0;
	}

	ad[channel].moving_avg.avg[ad[channel].moving_avg.index] = in;
	ad[channel].moving_avg.index = (ad[channel].moving_avg.index + 1)
			% MOVING_AVG_QUEUE_SIZE;

	uint32_t sum = 0;
	for (uint8_t i = 0; i < MOVING_AVG_QUEUE_SIZE; i++)
		sum += ad[channel].moving_avg.avg[i];

	return sum / MOVING_AVG_QUEUE_SIZE;
}

static uint16_t moving_hist(uint16_t in) {
	int channel = 0;  // Declare the 'channel' variable

	if (!chk_ch(channel)) {
		return 0;
	}
	if (in >= ad->moving_hist.min && in <= ad->moving_hist.max)
		return ad->moving_hist.min;

	ad->moving_hist.max =
			(in + ad->moving_hist.delta) >= AD_RAW_MAX ?
					AD_RAW_MAX : in + ad->moving_hist.delta;
	ad->moving_hist.min =
			((int) in) - ((int) ad->moving_hist.delta) <= 0 ?
					AD_RAW_MIN : in - MOVING_HIST_DELTA;

	return ad->moving_hist.min;
}

static void calc_temp(uint16_t in, int channel) {
	if (!ad[channel].calibration.valid)
		return;

	if (xSemaphoreTake(ad[channel].semaphore, pdMS_TO_TICKS(10)) == pdPASS) {
		float tmp = in * ad[channel].calibration.tg_alpha;
		ad[channel].thermal_value = tmp;
		xSemaphoreGive(ad[channel].semaphore);
	} else {
		++ad[channel].semaphore_error;
	}
}

static void tsk_ad(void *p) {
	int channel;  // Declare the 'channel' variable

	ESP_LOGD(TAG, "Enter tsk_ad for channel");

	for (;;) {
		vTaskDelay(pdMS_TO_TICKS(AD_CONVERSION_MS));
		for (channel = 0; channel < MAX_CHANNELS; channel++) {
			if (!ad[channel].calibration.valid) {
				ESP_LOGE(TAG,
						"A/D channel is not calibrated, please calibrate it!");
				continue;
			}

			if (adc_oneshot_read(adc_handle,
					(channel == 0) ? ADC_CHANNEL_0 : ADC_CHANNEL_2,
					&ad[channel].raw) == ESP_OK) {
				ad[channel].normalized = moving_hist(ad[channel].raw);
				// No need to pass 'channel' to moving_avg and moving_hist here
				calc_temp(ad[channel].normalized, channel);
			} else {
				++ad[channel].ad_errors;
			}

			ESP_LOGD(TAG, "After ADC read for channel %d", channel);
		}
	}
}

static int validate_calibrate(int channel) {
	float temp_min, temp_max;

	if (channel == 0) {
		temp_min = INNER_TEMP_MIN;
		temp_max = INNER_TEMP_MAX;
	} else if (channel == 1) {
		temp_min = OUTER_TEMP_MIN;
		temp_max = OUTER_TEMP_MAX;
	} else {
		// Handle invalid channel
		return 0;
	}

	return (ad[channel].calibration.d0 >= AD_RAW_MIN)
			&& (ad[channel].calibration.d0 <= AD_RAW_MAX)
			&& (ad[channel].calibration.d1 >= AD_RAW_MIN)
			&& (ad[channel].calibration.d1 <= AD_RAW_MAX)
			&& (ad[channel].calibration.d1 > ad[channel].calibration.d0)
			&& (ad[channel].calibration.temp_min >= temp_min)
			&& (ad[channel].calibration.temp_max <= temp_max)
			&& (ad[channel].calibration.temp_max >= temp_min)
			&& (ad[channel].calibration.temp_min <= temp_max) && // Corrected this line
			(ad[channel].calibration.temp_max > ad[channel].calibration.temp_min);
}

static void restore_flash(int channel) {
	if (!chk_ch(channel))
		return;

	char key_d0[10], key_d1[10], key_t0[10], key_t1[10];
	snprintf(key_d0, sizeof(key_d0), "%s_%d", NVS_KEY_D0, channel);
	snprintf(key_d1, sizeof(key_d1), "%s_%d", NVS_KEY_D1, channel);
	snprintf(key_t0, sizeof(key_t0), "%s_%d", NVS_KEY_T0, channel);
	snprintf(key_t1, sizeof(key_t1), "%s_%d", NVS_KEY_T1, channel);
	nvs_handle_t handle;
	esp_err_t err = nvs_open(NVS_NAME_SPACE, NVS_READONLY, &handle);
	if (err != ESP_OK) {
		printf("Nobody calibrated channel %d yet\r\n", channel);
		return;
	}

	if (nvs_get_u16(handle, key_d0, &ad[channel].calibration.d0) != ESP_OK) {
		goto exit;
	}

	if (nvs_get_u16(handle, key_d1, &ad[channel].calibration.d1) != ESP_OK) {
		goto exit;
	}

	uint16_t v;
	if (nvs_get_u16(handle, key_t0, &v) != ESP_OK) {
		goto exit;
	}

	ad[channel].calibration.temp_min = v / 100;

	if (nvs_get_u16(handle, key_t1, &v) != ESP_OK) {
		goto exit;
	}

	ad[channel].calibration.temp_max = v / 100;

	ad[channel].calibration.valid = false;

	uint16_t divider = ad[channel].calibration.d1 - ad[channel].calibration.d0;
	if (divider == 0)
		goto exit;

	ad[channel].calibration.tg_alpha = (ad[channel].calibration.temp_max
			- ad[channel].calibration.temp_min) / divider;
	validate_calibrate(channel);

	exit: nvs_close(handle);
}

BaseType_t ad_init() {
	esp_log_level_set(TAG, LOG_LOCAL_LEVEL);

	adc_oneshot_unit_init_cfg_t init_config1 = { .unit_id = ADC_UNIT_1, };
	ESP_ERROR_CHECK(adc_oneshot_new_unit(&init_config1, &adc_handle));

	adc_oneshot_chan_cfg_t config = { .bitwidth = ADC_BITWIDTH_12, .atten =
	ADC_ATTEN, };

	for (int i = 0; i < MAX_CHANNELS; i++) {
		ESP_ERROR_CHECK(
				adc_oneshot_config_channel(adc_handle, Channel_IDS[i],
						&config));

		int v;
		adc_oneshot_read(adc_handle, Channel_IDS[i], &v);
		memset(ad[i].moving_avg.avg, v,
		MOVING_AVG_QUEUE_SIZE * sizeof(uint16_t));
		ad[i].moving_hist.min = v - MOVING_HIST_DELTA;
		ad[i].moving_hist.max = v + MOVING_HIST_DELTA;
		ad[i].moving_hist.delta = MOVING_HIST_DELTA;
		ad[i].semaphore = xSemaphoreCreateBinary();
		restore_flash(i);
		xSemaphoreGive(ad[i].semaphore);
	}

	register_cmd();

	return xTaskCreate(tsk_ad, "A/D", 2048, NULL, uxTaskPriorityGet(NULL),
	NULL);
}

BaseType_t ad_deinit() {
	for (int i = 0; i < MAX_CHANNELS; i++) {

		if (ad[i].semaphore != NULL) {
			vSemaphoreDelete(ad[i].semaphore);
		}
	}

	return pdPASS;
}
void esp_efuse_mac_get_default(uint8_t *mac);
// Declaration of the function ad_get_temperature
BaseType_t ad_get_temperature(float *temperature);

// Declaration of the function send_mqtt
BaseType_t send_mqtt(const char *topic, const char *message);

BaseType_t ad_send(uint8_t size) {
	if (size == 0) {
		ESP_LOGE(TAG, "size is 0");
		return pdFAIL;
	}

	BaseType_t result = pdFAIL;

	cJSON *root = cJSON_CreateObject();
	if (!root) {
		ESP_LOGW(TAG, "root create error");
		return pdFAIL;
	}

	mac_t mac;
	esp_efuse_mac_get_default(mac.as_bytes);
	char mac_address[30];
	snprintf(mac_address, sizeof(mac_address), "%lld", mac.as_long);

	cJSON_AddStringToObject(root, JSON_LABEL_ESP_ADDRESS, mac_address);

	float temperature;
	if (ad_get_temperature(&temperature) == pdPASS) {
		cJSON_AddNumberToObject(root, JSON_LABEL_TEMPERATURE, temperature);
	} else {
		ESP_LOGE(TAG, "Failed to get temperature");
		cJSON_Delete(root);
		return pdFAIL;
	}

	char *str = cJSON_Print(root);
	if (!str) {
		ESP_LOGE(TAG, "json print");
		cJSON_Delete(root);
		return pdFAIL;
	}

	ESP_LOGD(TAG, "jsonstring is %s", str);

	result = send_mqtt(TOPIC_OBIS, str);

	cJSON_Delete(root);
	return result;
}

BaseType_t ad_get(int channel, float *value, TickType_t wait) {
	if (!value || !chk_ch(channel))
		return pdFAIL;

	if (xSemaphoreTake(ad[channel].semaphore, wait) == pdPASS) {
		*value = ad[channel].thermal_value;
		xSemaphoreGive(ad[channel].semaphore);
		return pdPASS;
	}

	return pdFAIL;
}
#define CMD_AD "ad"

//ad [--channel <number>] [--stat <--channel>] [--calibrate <--channel> --point <d0|d1> --temp <double>] [--help][--flash <-channel>]

static struct {
	arg_lit_t *help;
	arg_int_t *channel;
	arg_lit_t *stat;
	arg_str_t *point;
	arg_dbl_t *temp;
	arg_lit_t *flash;
	arg_lit_t *calibrate;
	arg_end_t *end;
} arg_ad;

static void print_help() {
	arg_print_syntax(stdout, (void*) &arg_ad, "\r\n");
	arg_print_glossary(stdout, (void*) &arg_ad, "%-25s %s\r\n");
}

static void print_avg(int channel) {
	printf("MOVING avg, index:%d", ad[channel].moving_avg.index);
	for (int i = 0; i < MOVING_AVG_QUEUE_SIZE; i++)
		printf("%u%s", ad[channel].moving_avg.avg[i],
				i < MOVING_AVG_QUEUE_SIZE - 1 ? "," : "\r\n");
}

static void print_stat(int channel) {
	if (!chk_ch(channel))
		return;

	printf("raw: %4d\tnormalized:%4d\tthermal:%5.2f\r\n", ad[channel].raw,
			ad[channel].normalized, ad[channel].thermal_value);
	printf("errors: %lu\r\n", ad[channel].ad_errors);
	print_avg(channel);
	printf("Moving hist min: %d, max:%d, delta:%d\r\n",
			ad[channel].moving_hist.min, ad[channel].moving_hist.max,
			ad[channel].moving_hist.delta);
	printf("Calibration d0:%d, d1:%d, t0:%f, t1:%f, tg alpha:%f, valid:%s\r\n",
			ad[channel].calibration.d0, ad[channel].calibration.d1,
			ad[channel].calibration.temp_min, ad[channel].calibration.temp_max,
			ad[channel].calibration.tg_alpha,
			ad[channel].calibration.valid ? "true" : "false");
}

static void print_flash(int channel) {
	if (!chk_ch(channel))
		return;

	char key_d0[10], key_d1[10], key_t0[10], key_t1[10];
	snprintf(key_d0, sizeof(key_d0), "%s_%d", NVS_KEY_D0, channel);
	snprintf(key_d1, sizeof(key_d1), "%s_%d", NVS_KEY_D1, channel);
	snprintf(key_t0, sizeof(key_t0), "%s_%d", NVS_KEY_T0, channel);
	snprintf(key_t1, sizeof(key_t1), "%s_%d", NVS_KEY_T1, channel);
	nvs_handle_t handle;
	esp_err_t err = nvs_open(NVS_NAME_SPACE, NVS_READONLY, &handle);
	if (err != ESP_OK) {
		printf("Nobody calibrated this channel (%d) yet\r\n", channel);
		return;
	}

	uint16_t d0;
	if (nvs_get_u16(handle, key_d0, &d0) != ESP_OK) {
		goto exit;
	}

	uint16_t d1;
	if (nvs_get_u16(handle, key_d1, &d1) != ESP_OK) {
		goto exit;
	}

	uint16_t t0;
	if (nvs_get_u16(handle, key_t0, &t0) != ESP_OK) {
		goto exit;
	}

	uint16_t t1;
	if (nvs_get_u16(handle, key_t1, &t1) != ESP_OK) {
		goto exit;
	}

	uint16_t ta;
	if (nvs_get_u16(handle, NVS_KEY_TG_ALPHA, &ta) != ESP_OK) {
		goto exit;
	}

	printf("d0:%d. d1:%d, t0:%f t1:%f , tg alpha:%f\r\n", d0, d1, t0 / 100.0,
			t1 / 100.0, ta / 100.0);

	exit: nvs_close(handle);
}

static void store_calibrate(int channel, float temp, int point) {
	if (!chk_ch(channel) || point < 0 || point > 1) {
		printf("Invalid channel or calibration point\r\n");
		return;
	}

	char key_d[20], key_t[20];
	snprintf(key_d, sizeof(key_d), "%s_%d",
			(point == 0) ? NVS_KEY_D0 : NVS_KEY_D1, channel);
	snprintf(key_t, sizeof(key_t), "%s_%d",
			(point == 0) ? NVS_KEY_T0 : NVS_KEY_T1, channel);

	printf("Debug: Storing calibration data. Key_d: %s, Key_t: %s\r\n", key_d,
			key_t);

	nvs_handle_t handle;
	esp_err_t err = nvs_open(NVS_NAME_SPACE, NVS_READWRITE, &handle);
	if (err != ESP_OK) {
		printf("Cannot open %s to read/write\r\n", NVS_NAME_SPACE);
		return;
	}

	int v;
	if (adc_oneshot_read(adc_handle, channel, &v) != ESP_OK) {
		printf("Error reading ADC value\r\n");
		goto exit_store;
	}

	// Store the digital value in NVS
	if (nvs_set_u16(handle, key_d, (uint16_t) v) != ESP_OK) {
		printf("Cannot store %s value to flash\r\n", key_d);
		goto exit_store;
	}

	// Store the temperature value in NVS (multiplied by 100 for precision)
	if (nvs_set_u16(handle, key_t, (uint16_t) (temp * 100.0)) != ESP_OK) {
		printf("Cannot store %s value to flash\r\n", key_t);
		goto exit_store;
	}

	exit_store: nvs_close(handle);
}

static int calibrate(int channel, const char *point, float temp) {
	if (!chk_ch(channel))
		return 1;

	float temp_min1, temp_max1;

	if (channel == 0) {
		temp_min1 = INNER_TEMP_MIN;
		temp_max1 = INNER_TEMP_MAX;
	} else if (channel == 1) {
		temp_min1 = OUTER_TEMP_MIN;
		temp_max1 = OUTER_TEMP_MAX;
	} else {
		printf("Invalid channel: %d\r\n", channel);
		return 1;
	}

	if (temp < temp_min1 || temp > temp_max1) {
		printf("Temperature must be between %f and %f\r\n", temp_min1,
				temp_max1);
		return 1;
	}

	if (strcasecmp("d0", point) == 0) {
		store_calibrate(channel, temp, 0);
		validate_calibrate(channel);
		return 0;
	}

	if (strcasecmp("d1", point) == 0) {
		store_calibrate(channel, temp, 1);
		validate_calibrate(channel);
		return 0;
	}

	printf("Invalid calibration point: %s\r\n", point);
	return 1;
}

static int cmd_ad(int argc, char **argv) {
	int errs = arg_parse(argc, argv, (void*) &arg_ad);
	if (errs) {
		print_help();
		return 1;
	}
	if (arg_ad.channel->count == 0) {
		printf("This command needs a channel between 0 up to %d\r\n",
		MAX_CHANNELS - 1);
		print_help();
		return 1;
	}
	int channel = *arg_ad.channel->ival;
	if (channel < 0 || channel >= MAX_CHANNELS) {
		printf("Invalid channel: %d\r\n", channel);
		print_help();
		return 1;
	}
	if (arg_ad.stat->count > 0) {
		print_stat(channel);
		return 0;
	}
	if (arg_ad.flash->count > 0) {
		print_flash(channel);
		return 0;
	}

	if (arg_ad.calibrate->count > 0) {
		const char *point = arg_ad.point->sval[0];
		if (!point) {
			printf("Calibrate needs a point d0 or d1\r\n");
			print_help();
			return 1;
		}

		printf("Calibrating with point: %s\r\n", point);

		if (strcasecmp("d0", point) == 0 || strcasecmp("d1", point) == 0) {
			if (arg_ad.temp->count > 0) {
				float temp = *arg_ad.temp->dval;
				printf("Temperature provided: %f\r\n", temp);
				return calibrate(channel, point, temp);
			} else {
				printf("Temperature argument missing\r\n");
				return 1;
			}
		}

		print_help();
		return 1;
	}

	print_help();
	return 0;
}

static int register_cmd() {
	arg_ad.help = arg_lit0("h", "help", "print this is help for A/D");
	arg_ad.channel = arg_int1("c", "channel", "<n>", "channel index");
	arg_ad.stat = arg_lit0("s", "status", "print A/D channel status");
	arg_ad.point = arg_str0("p", "point", "d0|d1",
			"min or max point of digital value");
	arg_ad.temp = arg_dbl0("t", "temp", "<f>",
			"temperature belong to digital input (d0|d1");
	arg_ad.flash = arg_lit0("f", "flash",
			"show stored params in flash for a channel");
	arg_ad.calibrate = arg_lit0(NULL, "calibrate", "calibrate a channel");
	arg_ad.end = arg_end(0);

	esp_console_cmd_t cmd = { .argtable = &arg_ad, .command = CMD_AD, .func =
			&cmd_ad, .help = "A/D commands" };

	return (esp_console_cmd_register(&cmd));
}

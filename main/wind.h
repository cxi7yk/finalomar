#ifndef WIND_SPEED_SENSOR_H
#define WIND_SPEED_SENSOR_H

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/semphr.h>
#include <driver/gpio.h>
#include <esp_console.h>
#include <esp_log.h>

#define WIND_SENSOR_PIN GPIO_NUM_17

typedef struct {
	uint32_t impulse_count;
	float wind_speed_kmh;
	float current_wind_speed;
} WindSpeedData;

void wind_speed_sensor_init();
void IRAM_ATTR wind_speed_isr_handler(void* arg);
void wind_speed_task(void *pvParameters);
int get_wind_speed(int argc, char **argv);
int reset_wind_speed(int argc, char **argv);
void register_commands();

#endif  // WIND_SPEED_SENSOR_H

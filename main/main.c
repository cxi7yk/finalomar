#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "nvs_flash.h"
#include "console.h"
#include "ad.h"
#include "wind.h"
#include "mqtt.h"

void app_main(void) {
	esp_err_t err = nvs_flash_init();
	if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
		ESP_ERROR_CHECK(nvs_flash_erase());
		err = nvs_flash_init();
	}
	ESP_ERROR_CHECK(err);

	// Initialize console
	configASSERT(con_init());

	// Initialize ADC sensor
	configASSERT(ad_init());

	// Initialize Wind sensor
	wind_speed_sensor_init();

}
